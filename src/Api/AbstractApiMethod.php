<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Api;

use Amass\Esputnik\Client\ClientInterface;
use Amass\Esputnik\Http\Response\Mediator;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\SerializerBuilder;

class AbstractApiMethod implements ApiMethodInterface
{

  /**
   * @var ClientInterface
   */
  protected $client;

  /**
   * @param ClientInterface $client
   */
  public function __construct(ClientInterface $client)
  {
    $this->client = $client;
  }

  /**
   * Send a GET request with query parameters.
   *
   * @param string $path Request path.
   * @param array $query GET parameters.
   *
   * @param array $parameters
   * @return array
   */
  protected function get($path, array $query = [], $parameters = [])
  {
    $response = $this->client
      ->getHttpClient()
      ->get($path, $query, $parameters);

    return Mediator::getContent($response);
  }

  /**
   * Send a POST request with JSON-encoded parameters.
   *
   * @param string $path Request path.
   * @param array $parameters POST parameters to be JSON encoded
   *
   * @return \Psr\Http\Message\StreamInterface
   */
  protected function post($path, $parameters = [])
  {
    return $this->postRaw(
      $path,
      $this->createJsonBody($parameters)
    );
  }

  /**
   * Send a PUT request with JSON-encoded parameters.
   *
   * @param string $path Request path.
   * @param array $parameters POST parameters to be JSON encoded.
   *
   * @return \Psr\Http\Message\StreamInterface
   */
  protected function put($path, $parameters = [])
  {
    $response = $this->client
      ->getHttpClient()
      ->put($path, $this->createJsonBody($parameters));

    return Mediator::getContent($response);
  }

  /**
   * Send a DELETE request with JSON-encoded parameters.
   *
   * @param string $path           Request path.
   * @param array  $parameters     POST parameters to be JSON encoded.
   *
   * @return \Psr\Http\Message\StreamInterface
   */
  protected function delete($path, array $parameters = [])
  {
    $response = $this->client
      ->getHttpClient()
      ->delete($path, $this->createJsonBody($parameters));

    return Mediator::getContent($response);
  }


  /**
   * Send a PATCH request with JSON-encoded parameters.
   *
   * @param string $path Request path.
   * @param array $parameters POST parameters to be JSON encoded
   *
   * @return \Psr\Http\Message\StreamInterface
   */
  protected function patch($path, array $parameters = [])
  {
    // TODO: Implement patch() method.
  }

  /**
   * Send a POST request with raw data.
   *
   * @param string $path Request path.
   * @param $body                     array body.
   *
   * @return \Psr\Http\Message\StreamInterface
   */
  private function postRaw($path, $body)
  {
    $response = $this->client->getHttpClient()->post(
      $path,
      $body
    );

    return Mediator::getContent($response);
  }

  /**
   * Create a JSON encoded version of an array of parameters.
   * @param $parameters
   * @return mixed
   */
  protected function createJsonBody($parameters)
  {
    $serializer = SerializerBuilder::create()
      ->setPropertyNamingStrategy(new SerializedNameAnnotationStrategy(new IdenticalPropertyNamingStrategy()))
      ->build();

    return $serializer->serialize($parameters, 'json');
  }
}