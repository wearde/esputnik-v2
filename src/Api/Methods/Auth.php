<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Api\Methods;

use Amass\Esputnik\Api\AbstractApiMethod;

/**
 * Class Auth
 * @package Amass\Esputnik\Api\Methods
 */
class Auth extends AbstractApiMethod
{
  /**
   * @return \Psr\Http\Message\StreamInterface
   */
  public function token()
    {
        return $this->post('auth/token');
    }
}