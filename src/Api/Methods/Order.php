<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Api\Methods;

use Amass\Esputnik\Api\AbstractApiMethod;

class Order extends AbstractApiMethod
{
  /**
   * Add orders.
   * Method POST
   * @see https://esputnik.com/api/el_ns1_orders.html
   *
   * @param \Amass\Esputnik\Models\Order[] $order
   * @return \Psr\Http\Message\StreamInterface
   */
  public function orders(array $order)
  {
    return $this->post('orders/', $order);
  }
}