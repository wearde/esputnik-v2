<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Api\Methods;

use Amass\Esputnik\Api\AbstractApiMethod;

/**
 * Class Addressbooks
 * @package Amass\Esputnik\Api\Methods
 */
class Addressbooks extends AbstractApiMethod
{
    /**
     * Get organisation balance.
     * Method GET
     * @see https://esputnik.com/api/el_ns0_balance.html
     * @return array
     */
    public function all()
    {
        return $this->get('addressbooks/');
    }
}