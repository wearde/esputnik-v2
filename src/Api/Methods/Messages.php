<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Api\Methods;

use Amass\Esputnik\Api\AbstractApiMethod;
use Amass\Esputnik\Models\EmailMessage;

class Messages extends AbstractApiMethod
{
  /**
   * @param EmailMessage $emailMessage
   * @return \Psr\Http\Message\StreamInterface
   */
  public function add(EmailMessage $emailMessage)
  {
    return $this->post('messages/email/', $emailMessage);
  }

  /**
   * @param array $parameters
   * @return \Psr\Http\Message\StreamInterface|array
   */
  public function all($parameters = [])
  {
    return $this->get('messages/email/', [], $parameters);
  }

  /**
   * @param $searchQuery
   * @param array $parameters
   * @return \Psr\Http\Message\StreamInterface|array
   */
  public function search($searchQuery, $parameters = [])
  {
    return $this->get('messages/email/', ['search' => $searchQuery], $parameters);
  }

  /**
   * Get email message.
   *
   * @param $id
   * @return \Psr\Http\Message\StreamInterface|array
   */
  public function show($id)
  {
    return $this->get('messages/email/' . rawurlencode($id));
  }

  /**
   * Delete email message.
   *
   * @param $id
   * @return \Psr\Http\Message\StreamInterface|array
   */
  public function remove($id)
  {
    return $this->delete('messages/email/' . rawurlencode($id));
  }

  /**
   * Update email message.
   *
   * @param $id
   * @param EmailMessage $emailMessage
   * @return \Psr\Http\Message\StreamInterface|array
   */
  public function update($id, EmailMessage $emailMessage)
  {
    return $this->put('messages/email/' . rawurlencode($id), $emailMessage);
  }
}