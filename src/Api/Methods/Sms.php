<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Api\Methods;

use Amass\Esputnik\Api\AbstractApiMethod;

class Sms extends AbstractApiMethod
{
  /**
   * @param array $parameters
   * @return \Psr\Http\Message\StreamInterface|array
   */
  public function all($parameters = [])
  {
    return $this->get('messages/sms', [], $parameters);
  }


  /**
   * Search sms messages by part of name or tag.
   *
   * @param $query
   * @param array $parameters
   * @return \Psr\Http\Message\StreamInterface|array
   */
  public function search($query, $parameters = [])
  {
    return $this->get('messages/sms', ['search' => $query], $parameters);
  }
}