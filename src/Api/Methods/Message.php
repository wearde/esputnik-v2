<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Api\Methods;

use Amass\Esputnik\Api\AbstractApiMethod;
use Amass\Esputnik\Exception\ErrorException;
use Amass\Esputnik\Models\Contact;
use Amass\Esputnik\Models\Group;
use Amass\Esputnik\Models\MessageParam;
use Amass\Esputnik\Models\ParamRecipient;

class Message extends AbstractApiMethod
{
  /**
   * Отправка рассылки по заранее созданному сообщению. Сообщение может дополнительно параметризироваться.
   *
   * @param int             $id
   * @param Contact    $contact
   * @param boolean         $isEmail
   * @param MessageParam[]  $params
   * @param string          $fromName
   * @param array|null      $recipients
   * @param Group|null $group
   * @return \Psr\Http\Message\StreamInterface
   */
  public function send(
    $id,
    Contact $contact,
    $isEmail,
    array $params,
    $fromName,
    array $recipients = null,
    Group $group = null
  ) {

    $queryParams = [
      'contactId' => $contact->getId(),
      'email' => (bool) $isEmail,
      'params' => $params,
      'fromName' => $fromName,
    ];

    if ($group) {
      $queryParams['groupId'] = $group->getId();
    }

    if ($recipients) {
      $queryParams['recipients'] = $recipients;
    }

    return $this->post('message/'.rawurlencode($id).'/send', $queryParams);
  }

  /**
   * Отправка подготовленного сообщения одному или многим контактам.
   * Сообщение может параметризироваться для каждого контакта отдельно.
   *
   * @param int                     $id
   * @param ParamRecipient[] $recipients
   * @param boolean                 $isEmail
   * @param string                  $fromName
   * @return string
   */
  public function smartSend($id, array $recipients, $isEmail, $fromName)
  {
    $queryParams = [
      'recipients' => $recipients,
      'email' => (bool) $isEmail,
      'fromName' => $fromName
    ];

    return $this->post('message/'.rawurlencode($id).'/smartsend', $queryParams);
  }

  /**
   * Отправить email-сообщение. Если контакта с таким адресом нет, он будет создан.
   *
   * @param $from
   * @param $subject
   * @param $htmlText
   * @param $plainText
   * @param array $emails
   * @param array $tags
   * @return \Psr\Http\Message\StreamInterface
   */
  public function email($from, $subject, $htmlText, $plainText, array $emails, array $tags)
  {
    $queryParams = [
      'from' => $from,
      'subject' => $subject,
      'htmlText' => $htmlText,
      'plainText' => $plainText,
      'emails' => $emails,
      'tags' => $tags,
    ];

    return $this->post('message/email/', $queryParams);
  }

  /**
   * Получить статус одиночного email сообщения.
   *
   * @param $ids
   * @return \Psr\Http\Message\StreamInterface|array
   */
  public function emailStatus($ids)
  {
    return $this->get('message/email/status/', [
      'ids' => join(',', $ids)
    ]);
  }

  /**
   * Отправить sms-сообщение. Если контакта с таким номером телефона нет, он будет создан.
   *
   * @param string $from
   * @param string $text
   * @param string[] $tags
   * @param string[] $phoneNumbers
   * @param int|null $groupId
   * @return \Psr\Http\Message\StreamInterface
   * @throws ErrorException
   */
  public function sms($from, $text, array $tags = [], array $phoneNumbers = [], $groupId = null)
  {
    $queryParams = [
      'from' => $from,
      'text' => $text,
    ];
    if (!$phoneNumbers && !$groupId) {
      throw new ErrorException(['phoneNumbers', 'groupId']);
    }

    if (!empty($phoneNumbers)) {
      $queryParams['phoneNumbers'] = $phoneNumbers;
    }
    if ($groupId) {
      $queryParams['groupId'] = $groupId;
    }
    if (!empty($tags)) {
      $queryParams['tags'] = $tags;
    }

    return $this->post('message/sms/', $queryParams);
  }

  /**
   * Получить статус одиночного sms сообщения.
   *
   * @param $ids
   * @return \Psr\Http\Message\StreamInterface|array
   */
  public function smsStatus($ids)
  {
    return $this->get('message/sms/status/', [
      'ids' => join(',', $ids)
    ]);
  }
}
