<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Api\Methods;

use Amass\Esputnik\Api\AbstractApiMethod;
use Amass\Esputnik\Http\Response\Models\Balance as ResponseBalance;

class Version extends AbstractApiMethod
{
    /**
     * Get organisation balance.
     * Method GET
     * @see https://esputnik.com/api/el_ns0_balance.html
     * @return array
     */
    public function show()
    {
        return $this->get('version/');
    }
}