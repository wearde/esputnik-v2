<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Api\Methods;

use Amass\Esputnik\Api\AbstractApiMethod;
use Psr\Http\Message\StreamInterface;

class Contact extends AbstractApiMethod
{

  /**
   * Get contact
   * Method GET
   * @see https://esputnik.com/api/el_ns0_contact.html
   * @param $id integer
   * @return array| StreamInterface
   */
  public function show($id)
  {
    return $this->get('contact/' . rawurlencode($id));
  }

  /**
   * Add new contact
   * Method POST
   * @see https://esputnik.com/api/el_ns0_contact.html
   * @param $contact \Amass\Esputnik\Models\Contact
   * @return array| StreamInterface
   */
  public function add($contact)
  {
    return $this->post('contact/', $contact);
  }

  /**
   * Get organisation balance.
   * Method PUT
   * @see https://esputnik.com/api/el_ns0_contactUpdate.html
   * @param $id integer
   * @param $contact \Amass\Esputnik\Models\Contact
   * @return array| StreamInterface
   */
  public function update($id, $contact)
  {
    return $this->put('contact/'. rawurlencode($id), $contact);
  }

  /**
   * Get organisation balance.
   * Method DELETE
   * @see https://esputnik.com/api/el_ns0_contact.html
   * @param $id integer
   * @return array| StreamInterface
   */
  public function remove($id)
  {
    return $this->delete('contact/'. rawurlencode($id));
  }
}