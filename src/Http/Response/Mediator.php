<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Http\Response;

use GuzzleHttp\Psr7\Response;

/**
 * Class Mediator
 * @package Amass\Esputnik\Http\Response
 */
class Mediator
{
    /**
     * @param Response $response
     * @param bool $asObject
     * @return \GuzzleHttp\Psr7\Stream|mixed|\Psr\Http\Message\StreamInterface
     */
    public static function getContent(Response $response, $asObject = false)
    {
        $contentType = $response->getHeader('Content-Type');

        $body = $response->getBody();
        if ($contentType and strpos($contentType[0], 'application/json') === 0) {
            $content = json_decode($body, true);
            if (JSON_ERROR_NONE === json_last_error()) {
                return $content;
            }
        }
        return $body;
    }
}