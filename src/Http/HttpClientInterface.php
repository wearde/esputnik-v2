<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Http;

/**
 * Interface HttpClientInterface
 * @package Amass\Esputnik\Http
 */
interface HttpClientInterface
{
    /**
     * HttpClientInterface constructor.
     * @param array $options
     */
    public function __construct(array $options);

    /**
     * @param $path
     * @param array $query
     * @param array $parameters
     * @return mixed
     */
    public function get($path, array $query = [], $parameters = []);

    /**
     * @param $path
     * @param null $body
     * @return mixed
     */
    public function post($path, $body = null);

    /**
     * @param $path
     * @param null $body
     * @return mixed
     */
    public function patch($path, $body = null);

    /**
     * @param $path
     * @param null $body
     * @return mixed
     */
    public function delete($path, $body = null);

    /**
     * @param $path
     * @param $body
     * @return mixed
     */
    public function put($path, $body);
}