<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Http;

use Amass\Esputnik\Exception\EsputnikClientException;
use Amass\Esputnik\Exception\EsputnikServerException;
use Amass\Esputnik\Exception\RuntimeException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class HttpClient implements HttpClientInterface
{
    /**
     * @var array
     */
    private $options;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var
     */
    protected $lastRequest;
    /**
     * @var
     */
    protected $lastResponse;

    /**
     * HttpClient constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->options = $options;
        $this->client = new Client($this->options);
    }
    /**
     * {@inheritDoc}
     */
    public function get($path, array $query = [], $parameters = [])
    {
        $this->options['query'] = $query;

        if (isset($parameters['maxrows'])) {
            $this->options['query']['maxrows'] = $parameters['maxrows'];
        }

        if (isset($parameters['startindex'])) {
            $this->options['query']['startindex'] = $parameters['startindex'];
        }

        return $this->request($path, null, 'GET');
    }

    /**
     * {@inheritDoc}
     */
    public function post($path, $body = null)
    {
        return $this->request($path, $body, 'POST');
    }

    /**
     * {@inheritDoc}
     */
    public function patch($path, $body = null)
    {
        return $this->request($path, $body, 'PATCH');
    }

    /**
     * {@inheritDoc}
     */
    public function delete($path, $body = null)
    {
        return $this->request($path, $body, 'DELETE');
    }

    /**
     * {@inheritDoc}
     */
    public function put($path, $body)
    {
        return $this->request($path, $body, 'PUT');
    }

    /**
     * @inheritDoc
     */
    public function request($path, $body = null, $httpMethod = 'GET', array $headers = [])
    {
        $request = $this->createRequest($httpMethod, $path, $body, $headers);

        try {
            $response = $this->client->send($request, $this->options);
        } catch (ClientException $e) {
            $eResponse = null;
            $eRequest = $e->getRequest();
            if ($e->hasResponse()) {
                $eResponse = $e->getResponse();
            }
            $message = sprintf(
                '%s: `%s %s` resulted in a `%s %s`',
                'Client Error',
                $eRequest->getMethod(),
                $eRequest->getUri(),
                $eResponse->getStatusCode(),
                $eResponse->getReasonPhrase()
            );
            throw new EsputnikClientException($message, $eResponse->getStatusCode(), $e);
        } catch (ServerException $e) {
            $eResponse = null;
            $eRequest = $e->getRequest();
            if ($e->hasResponse()) {
                $eResponse = $e->getResponse();
            }
            $message = sprintf(
                '%s: `%s %s` resulted in a `%s %s`',
                'Client Error',
                $eRequest->getMethod(),
                $eRequest->getUri(),
                $eResponse->getStatusCode(),
                $eResponse->getReasonPhrase()
            );
            throw new EsputnikServerException($message, $eResponse->getStatusCode(), $e);
        } catch (RequestException $e) {
            throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
        }

        $this->lastRequest = $request;
        $this->lastResponse = $response;

        return $response;
    }

    /**
     * @return Request
     */
    public function getLastRequest()
    {
        return $this->lastRequest;
    }

    /**
     * @return Response
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * @param $httpMethod
     * @param $path
     * @param null $body
     * @param array $headers
     * @return Request
     */
    protected function createRequest($httpMethod, $path, $body = null, array $headers = [])
    {
        return new Request(
            $httpMethod,
            $path,
            $headers,
            $body
        );
    }
}