<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Config;

use Amass\Esputnik\Base;

/**
 * Class Config
 * @package Amass\Esputnik\Config
 */
class Config extends Base
{
    /**
     * @var string
     */
    private $baseUrli = 'https://esputnik.com/api/';
    /**
     * @var string
     */
    private $apiVersion = 'v1';
    /**
     * @var string
     */
    private $userAgent = 'esputnik-api';
    /**
     * @var bool
     */
    private $debug = false;
    /**
     * @var null|mixed
     */
    private $cache = null;
    /**
     * @var null|string
     */
    private $cacheDir = null;
    /**
     * @var string
     */
    private $contentType = 'application/json; charset=UTF-8';

    private $options = [];

    private $headers = [];

    /**
     * Config constructor.
     * @param string $login
     * @param string $password
     */
    public function __construct($login, $password)
    {
        $this->options['auth'] = [$login, $password];
    }

    /**
     * @param string $baseUrli
     * @return Config
     */
    public function setBaseUrli($baseUrli)
    {
        $this->baseUrli = $baseUrli;
        return $this;
    }

    /**
     * @param string $userAgent
     * @return Config
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDebug()
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     * @return Config
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
        return $this;
    }

    /**
     * @param null $cache
     * @return Config
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * @param string $apiVersion
     * @return Config
     */
    public function setApiVersion($apiVersion)
    {
        $this->apiVersion = $apiVersion;
        return $this;
    }

    /**
     * @param null $cacheDir
     * @return Config
     */
    public function setCacheDir($cacheDir)
    {
        $this->cacheDir = $cacheDir;
        return $this;
    }

    /**
     * @param string $contentType
     * @return Config
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
        return $this;
    }

    /**
     * @param string $name
     * @param mixed $option
     * @return $this
     */
    public function setOption($name, $option)
    {
        $this->options[$name] = $option;
        return $this;
    }

    /**
     * @param string $header
     * @param mixed $value
     * @return $this
     */
    public function setHeader($header, $value)
    {
        $this->headers[$header] = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return array_merge($this->defaultHeaders(), $this->headers);
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return array_merge($this->defaultOptions(), $this->options);
    }

    /**
     *
     */
    private function defaultOptions()
    {
        return array_merge($this->options,
            [
                'base_uri' => sprintf('%s%s/', $this->baseUrli, $this->apiVersion),
                'debug' => $this->debug,
                'cache' => $this->cache,
                'cache_dir' => $this->cacheDir,
                'headers' => $this->getHeaders()
            ]
        );
    }

    /**
     *
     */
    private function defaultHeaders()
    {
        return [
            'Accept' => 'application/json',
            'User-Agent' => sprintf('%s', $this->userAgent),
            'Content-Type' => sprintf('%s', $this->contentType),
        ];
    }
}