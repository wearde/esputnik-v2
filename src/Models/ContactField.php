<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Models;

/**
 * Class ContactField
 *
 * @property int $id
 * @property string $value
 *
 * @link http://esputnik.com.ua/api/ns0_contactField.html
 */
class ContactField extends AbstractModel
{
  /**
   * @var array
   */
  protected $error;
  /**
   * @var int
   */
  protected $id;

  /**
   * @var string
   */
  protected $value;
}