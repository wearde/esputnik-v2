<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Models;

/**
 * Class Items
 * @package Amass\Esputnik\Models
 */
/**
 * Class Items
 * @package Amass\Esputnik\Models
 */
class Items extends AbstractModel
{
  /**
   * @var string ID of product in the external system (required).
   */
  protected $externalItemId;

  /**
   * @var string Product name (required).
   */
  protected $name;
   /**
   * @var string Product category (required).
   */
  protected $category;
   /**
   * @var integer Number of items (required).
   */
  protected $quantity;
   /**
   * @var double Product price (required).
   */
  protected $cost;
   /**
   * @var string Link to product page (required).
   */
  protected $url;
   /**
   * @var string Link to a product image.
   */
  protected $imageUrl;
   /**
   * @var string Short description of product.
   */
  protected $description;

  /**
   * @return string
   */
  public function getExternalItemId()
  {
    return $this->externalItemId;
  }

  /**
   * @param $externalItemId
   * @return $this
   */
  public function setExternalItemId($externalItemId)
  {
    $this->externalItemId = $externalItemId;
    return $this;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param $name
   * @return $this
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * @return string
   */
  public function getCategory()
  {
    return $this->category;
  }

  /**
   * @param $category
   * @return $this
   */
  public function setCategory($category)
  {
    $this->category = $category;
    return $this;
  }

  /**
   * @return int
   */
  public function getQuantity()
  {
    return $this->quantity;
  }

  /**
   * @param $quantity
   * @return $this
   */
  public function setQuantity($quantity)
  {
    $this->quantity = $quantity;
    return $this;
  }

  /**
   * @return float
   */
  public function getCost()
  {
    return $this->cost;
  }

  /**
   * @param $cost
   * @return $this
   */
  public function setCost($cost)
  {
    $this->cost = $cost;
    return $this;
  }

  /**
   * @return string
   */
  public function getUrl()
  {
    return $this->url;
  }

  /**
   * @param $url
   * @return $this
   */
  public function setUrl($url)
  {
    $this->url = $url;
    return $this;
  }

  /**
   * @return string
   */
  public function getImageUrl()
  {
    return $this->imageUrl;
  }

  /**
   * @param $imageUrl
   * @return $this
   */
  public function setImageUrl($imageUrl)
  {
    $this->imageUrl = $imageUrl;
    return $this;
  }

  /**
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param $description
   * @return $this
   */
  public function setDescription($description)
  {
    $this->description = $description;
    return $this;
  }
}