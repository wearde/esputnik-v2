<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Models;

class MessageParam
{
  private $key;
  private $value;

  /**
   * MessageParam constructor.
   * @param $key
   * @param $value
   */
  public function __construct($key, $value)
  {
    $this->key = $key;
    $this->value = $value;
  }

  /**
   * @return mixed
   */
  public function getKey()
  {
    return $this->key;
  }

  /**
   * @return mixed
   */
  public function getValue()
  {
    return $this->value;
  }
}
