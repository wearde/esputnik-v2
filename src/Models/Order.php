<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Models;

/**
 * Class Contact
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property Channel[] $channels
 * @property Address $address
 * @property ContactField[] $fields
 * @property int $addressBookId
 * @property string $contactKey
 * @property Group[] $groups
 *
 * @link https://esputnik.com/api/ns1_order.html
 */
/**
 * Class Order
 * @package Amass\Esputnik\Models
 */
/**
 * Class Order
 * @package Amass\Esputnik\Models
 */
class Order extends AbstractModel
{
  /**
   * @var array
   */
  private $error;

  /**
   * @var int
   */
  private $id;

  /**
   * @var string ID of order in the external system (required).
   */
  protected $externalOrderId;

  /**
   * @var string ID of customer in the external system (required).
   */
  protected $externalCustomerId;

  /**
   * @var double Total cost of order (required).
   */
  protected $totalCost;

  /**
   * @var string Order status (required).
   * Possible values: DELIVERED, IN_PROGRESS, CANCELLED, ABANDONED_SHOPPING_CART, INITIALIZED.
   * Only DELIVERED orders are used in RFM analysis.
   */
  protected $status;

  /**
   * @var string Order date and time (required).
   * For example "2017-05-14T10:11:10.7022176+03:00"
   */
  protected $date;

  /**
   * @var string Customer email address.
   */
  protected $email;

  /**
   * @var string Customer phone number.
   */
  protected $phone;

  /**
   * @var string Customer first name.
   */
  protected $firstName;

  /**
   * @var string Customer last name.
   */
  protected $lastName;

  /**
   * @var string Currency according to ISO 4217. For example USD, EUR, UAH.
   */
  protected $currency;

  /**
   * @var double Shipping cost.
   */
  protected $shipping;

  /**
   * @var double Discount.
   */
  protected $discount;

  /**
   * @var double Amount of tax.
   */
  protected $taxes;

  /**
   * @var string Order status description.
   */
  protected $statusDescription;

  /**
   * @var string Link to order.
   */
  protected $restoreUrl;
   /**
   * @var string Store ID (if you work with several stores in one eSputnik account).
   */
  protected $storeId;

   /**
   * @var string Source.
   */
  protected $source;

   /**
   * @var string  Delivery method.
   */
  protected $deliveryMethod;

   /**
   * @var string Payment method.
   */
  protected $paymentMethod;

   /**
   * @var string Delivery address.
   */
  protected $deliveryAddress;

   /**
   * @var Items[] Array of ordered products.
   */
  protected $items = [];

  /**
   * @return array
   */
  public function getError()
  {
    return $this->error;
  }

  /**
   * @param $error
   * @return $this
   */
  public function setError($error)
  {
    $this->error = $error;
    return $this;
  }

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param $id
   * @return $this
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * @return string
   */
  public function getExternalOrderId()
  {
    return $this->externalOrderId;
  }

  /**
   * @param $externalOrderId
   * @return $this
   */
  public function setExternalOrderId($externalOrderId)
  {
    $this->externalOrderId = $externalOrderId;
    return $this;
  }

  /**
   * @return string
   */
  public function getExternalCustomerId()
  {
    return $this->externalCustomerId;
  }

  /**
   * @param $externalCustomerId
   * @return $this
   */
  public function setExternalCustomerId($externalCustomerId)
  {
    $this->externalCustomerId = $externalCustomerId;
    return $this;
  }

  /**
   * @return float
   */
  public function getTotalCost()
  {
    return $this->totalCost;
  }

  /**
   * @param $totalCost
   * @return $this
   */
  public function setTotalCost($totalCost)
  {
    $this->totalCost = $totalCost;
    return $this;
  }

  /**
   * @return string
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * @param $status
   * @return $this
   */
  public function setStatus($status)
  {
    $this->status = $status;
    return $this;
  }

  /**
   * @return string
   */
  public function getDate()
  {
    return $this->date;
  }

  /**
   * @param $date
   * @return $this
   */
  public function setDate($date)
  {
    $this->date = $date;
    return $this;
  }

  /**
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @param $email
   * @return $this
   */
  public function setEmail($email)
  {
    $this->email = $email;
    return $this;
  }

  /**
   * @return string
   */
  public function getPhone()
  {
    return $this->phone;
  }

  /**
   * @param $phone
   * @return $this
   */
  public function setPhone($phone)
  {
    $this->phone = $phone;
    return $this;
  }

  /**
   * @return string
   */
  public function getFirstName()
  {
    return $this->firstName;
  }

  /**
   * @param $firstName
   * @return $this
   */
  public function setFirstName($firstName)
  {
    $this->firstName = $firstName;
    return $this;
  }

  /**
   * @return string
   */
  public function getLastName()
  {
    return $this->lastName;
  }

  /**
   * @param $lastName
   * @return $this
   */
  public function setLastName($lastName)
  {
    $this->lastName = $lastName;
    return $this;
  }

  /**
   * @return string
   */
  public function getCurrency()
  {
    return $this->currency;
  }

  /**
   * @param $currency
   * @return $this
   */
  public function setCurrency($currency)
  {
    $this->currency = $currency;
    return $this;
  }

  /**
   * @return float
   */
  public function getShipping()
  {
    return $this->shipping;
  }

  /**
   * @param $shipping
   * @return $this
   */
  public function setShipping($shipping)
  {
    $this->shipping = $shipping;
    return $this;
  }

  /**
   * @return float
   */
  public function getDiscount()
  {
    return $this->discount;
  }

  /**
   * @param $discount
   * @return $this
   */
  public function setDiscount($discount)
  {
    $this->discount = $discount;
    return $this;
  }

  /**
   * @return float
   */
  public function getTaxes()
  {
    return $this->taxes;
  }

  /**
   * @param $taxes
   * @return $this
   */
  public function setTaxes($taxes)
  {
    $this->taxes = $taxes;
    return $this;
  }

  /**
   * @return string
   */
  public function getStatusDescription()
  {
    return $this->statusDescription;
  }

  /**
   * @param $statusDescription
   * @return $this
   */
  public function setStatusDescription($statusDescription)
  {
    $this->statusDescription = $statusDescription;
    return $this;
  }

  /**
   * @return string
   */
  public function getRestoreUrl()
  {
    return $this->restoreUrl;
  }

  /**
   * @param $restoreUrl
   * @return $this
   */
  public function setRestoreUrl($restoreUrl)
  {
    $this->restoreUrl = $restoreUrl;
    return $this;
  }

  /**
   * @return string
   */
  public function getStoreId()
  {
    return $this->storeId;
  }

  /**
   * @param $storeId
   * @return $this
   */
  public function setStoreId($storeId)
  {
    $this->storeId = $storeId;
    return $this;
  }

  /**
   * @return string
   */
  public function getSource()
  {
    return $this->source;
  }

  /**
   * @param $source
   * @return $this
   */
  public function setSource($source)
  {
    $this->source = $source;
    return $this;
  }

  /**
   * @return string
   */
  public function getDeliveryMethod()
  {
    return $this->deliveryMethod;
  }

  /**
   * @param $deliveryMethod
   * @return $this
   */
  public function setDeliveryMethod($deliveryMethod)
  {
    $this->deliveryMethod = $deliveryMethod;
    return $this;
  }

  /**
   * @return string
   */
  public function getPaymentMethod()
  {
    return $this->paymentMethod;
  }

  /**
   * @param $paymentMethod
   * @return $this
   */
  public function setPaymentMethod($paymentMethod)
  {
    $this->paymentMethod = $paymentMethod;
    return $this;
  }

  /**
   * @return string
   */
  public function getDeliveryAddress()
  {
    return $this->deliveryAddress;
  }

  /**
   * @param $deliveryAddress
   * @return $this
   */
  public function setDeliveryAddress($deliveryAddress)
  {
    $this->deliveryAddress = $deliveryAddress;
    return $this;
  }

  /**
   * @return Items[]
   */
  public function getItems()
  {
    return $this->items;
  }

  /**
   * @param $items
   * @return $this
   */
  public function setItems($items)
  {
    $this->items = $items;
    return $this;
  }


}