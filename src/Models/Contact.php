<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Models;

/**
 * Class Contact
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property Channel[] $channels
 * @property Address $address
 * @property ContactField[] $fields
 * @property int $addressBookId
 * @property string $contactKey
 * @property Group[] $groups
 *
 * @link http://esputnik.com.ua/api/el_ns0_contact.html
 */
class Contact extends AbstractModel
{
  /**
   * @var array
   */
  protected $error;
  /**
   * @var int
   */
  protected $id;

  /**
   * @var string
   */
  protected $firstName;

  /**
   * @var string
   */
  protected $lastName;

  /**
   * @var Channel[]
   */
  protected $channels = array();

  /**
   * @var Address
   */
  protected $address;

  /**
   * @var ContactField[]
   */
  protected $fields = array();

  /**
   * @var int
   */
  protected $addressBookId;

  /**
   * @var string
   */
  protected $contactKey;

  /**
   * @var Group[]
   */
  protected $groups = array();

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @param Address $address
   */
  public function setAddress($address)
  {
    $this->address = $address instanceof Address ? $address : new Address($address);
  }

  /**
   * @param Channel[] $channels
   */
  public function setChannels(array $channels)
  {
    $this->channels = array_map(function ($channel) {
      return $channel instanceof Channel ? $channel : new Channel($channel);
    }, $channels);
  }

  /**
   * @param ContactField[] $fields
   */
  public function setFields(array $fields)
  {
    $this->fields = array_map(function ($field) {
      return $field instanceof ContactField ? $field : new ContactField($field);
    }, $fields);
  }

  /**
   * @param Group[] $groups
   */
  public function setGroups(array $groups)
  {
    $this->groups = array_map(function ($group) {
      return $group instanceof Group ? $group : new Group($group);
    }, $groups);
  }

  /**
   * @param string $type
   * @param string $value
   * @return Channel
   */
  public function addChannel($type, $value)
  {
    return $this->channels[] = new Channel($type, $value);
  }

  /**
   * @return string[int]
   */
  public function fieldsById()
  {
    return array_reduce($this->fields, function ($result, ContactField $field) {
      $result[$field->id] = $field->value;
      return $result;
    }, []);
  }
}