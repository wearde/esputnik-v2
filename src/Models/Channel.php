<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Models;

/**
 * Class Channel
 *
 * @property string $type
 * @property string $value
 *
 * @link http://esputnik.com.ua/api/el_ns0_channel.html
 */
class Channel extends AbstractModel
{
  /**
   * @var array
   */
  protected $error;
  /**
   * @var string
   */
  protected $type;

  /**
   * @var string
   */
  protected $value;

  /**
   * Channel constructor.
   * @param $type
   * @param $value
   */
  public function __construct($type, $value)
  {
    $this->type = $this->setType($type);
    $this->value = $value;
    return $this;
  }

  /**
   * Set the type value
   * @param $type
   * @return string
   * @throws \Exception
   */
  public function setType($type)
  {
    static $values = array(
      'email',
      'sms'
    );

    if (!in_array($type, $values)) {
      throw new \Exception('Property type must be one of ' . implode(', ', $values) . ' values.');
    }

    $this->type = $type;
    return $this->type;
  }
}