<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Models;

class OrderStatus
{
  const INITIALIZED = 'INITIALIZED';
  const IN_PROGRESS = 'IN_PROGRESS';
  const DELIVERED = 'DELIVERED';
  const CANCELLED = 'CANCELLED';
  const ABANDONED_SHOPPING_CART = 'ABANDONED_SHOPPING_CART';
}