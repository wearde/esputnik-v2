<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Models;

use Amass\Esputnik\Exception\ErrorException;
/**
 * Class Group
 *
 * @property int $id
 * @property string $name
 * @property string $type
 *
 * @link http://esputnik.com.ua/api/el_ns0_group.html
 */
class Group extends AbstractModel
{
  /**
   * @var array
   */
  protected $error;
  /**
   * @var int
   */
  protected $id;

  /**
   * @var string
   */
  protected $name;

  /**
   * @var string
   */
  protected $type;

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * Set the type value
   *
   * @param string $type
   * @throws \Exception
   */
  public function setType($type)
  {
    static $values = array(
      'Static',
      'Dynamic',
      'Combined'
    );

    if (!in_array($type, $values)) {
      throw new ErrorException('Property type must be one of ' . implode(', ', $values) . ' values.');
    }

    $this->type = $type;
  }
}