<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Models;

/**
 * Class Address
 *
 * @property string $region
 * @property string $town
 * @property string $address
 * @property string $postcode
 *
 * @link http://esputnik.com.ua/api/el_ns0_address.html
 */
class Address extends AbstractModel
{
  /**
   * @var array
   */
  protected $error;
  /**
   * @var string
   */
  protected $region;

  /**
   * @var string
   */
  protected $town;

  /**
   * @var string
   */
  protected $address;

  /**
   * @var string
   */
  protected $postcode;
}
