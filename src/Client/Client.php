<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Client;

use Amass\Esputnik\Api\AbstractApiMethod;
use Amass\Esputnik\Api\Methods\Balance;
use Amass\Esputnik\Base;
use Amass\Esputnik\Config\Config;
use Amass\Esputnik\Exception\InvalidApiMethodException;
use Amass\Esputnik\Http\HttpClient;

/**
 * Class Client
 * @package Amass\Esputnik\Api\Client
 */
class Client extends Base implements ClientInterface
{
    /**
     * @var Config
     */
    private $options;

    /**
     * @var null
     */
    private $httpClient;

    /**
     * @var string
     */
    private $methodsNamespace;

    /**
     * Client constructor.
     * @param Config $options
     * @param null $httpClient
     * @param string $methodsNamespace
     */
    public function __construct(Config $options, $httpClient = null, $methodsNamespace = 'Amass\Esputnik\Api\Methods\\')
    {
        $this->options = $options->getOptions();
        $this->httpClient = $httpClient;
        $this->methodsNamespace = $methodsNamespace;
    }

    /**
     * @return mixed
     */
    public function getHttpClient()
    {
        if (null === $this->httpClient) {
            $this->httpClient = new HttpClient($this->options);
        }
        return $this->httpClient;
    }

    /**
     * @param $method
     * @return Balance
     * @throws InvalidApiMethodException
     * @todo try do it with factory
     */
//    private function api($method)
//    {
//        $methodClass = $this->methodsNamespace . ucwords($method);
//        if(!class_exists($methodClass)){
//            throw new InvalidApiMethodException('Not found implemented ' . $method . '. Should been created class ' . $methodClass);
//        }
//        return new $methodClass($this);
//    }
}