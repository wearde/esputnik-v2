<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Client;

use Amass\Esputnik\Config\Config;

interface ClientInterface
{
    /**
     * ClientInterface constructor.
     * @param null $httpClient
     * @param Config $options
     */
    public function __construct(Config $options, $httpClient = null);

    /**
     * @return mixed
     */
    public function getHttpClient();
}