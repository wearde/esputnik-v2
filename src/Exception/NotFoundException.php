<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Exception;

/**
 * Class NotFoundException
 * @package Amass\Esputnik\Exception
 */
class NotFoundException extends ErrorException
{
    public function __construct($code = 404, \Exception $previous = null)
    {
        parent::__construct('Resulted in a 401 Unauthorized! Invalid username or password', $code, $previous);
    }
}