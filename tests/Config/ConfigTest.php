<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Tests\Config;

use Amass\Esputnik\Config\Config;

/**
 * Class ConfigTest
 * @package Amass\Esputnik\Tests\Config
 */
class ConfigTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Config $config
     */
    public $config;

    public function setUp()
    {
        $this->config = new Config('user', 'password');
    }



    public function testSetOption()
    {
        $this->config->setOption('demo', 'value');
        $this->assertArrayHasKey('demo', $this->config->getOptions());
    }

    public function testSetHeader()
    {
        $this->config->setHeader('demoHeader', 'value');
        $this->assertArrayHasKey('demoHeader', $this->config->getHeaders());
    }

    public function testUri()
    {
        $uri = 'uri';
        $version = 'v1';
        $this->config->setBaseUrli($uri);
        $this->config->setApiVersion($version);
        $this->assertEquals($uri.$version . '/', $this->config->getOptions()['base_uri']);
    }

    public function testDebug()
    {
        $this->config->setDebug(true);
        $this->assertTrue($this->config->isDebug());
    }

    public function testCache()
    {
        $this->config->setCache(true);
        $this->assertEquals(true, $this->config->getOptions()['cache']);
    }

    public function testCacheDir()
    {
        $this->config->setCacheDir('dir');
        $this->assertEquals('dir', $this->config->getOptions()['cache_dir']);
    }

    public function testUserAgent()
    {
        $this->config->setUserAgent('agent');
        $this->assertEquals('agent', $this->config->getHeaders()['User-Agent']);
    }

    public function testContentType()
    {
        $this->config->setContentType('content-type');
        $this->assertEquals('content-type', $this->config->getHeaders()['Content-Type']);
    }
}
