<?php
/**
 * This file is part of the "Esputnik" API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Esputnik\Tests\Client;

use Amass\Esputnik\Client\Client;
use Amass\Esputnik\Config\Config;
use Amass\Esputnik\Http\HttpClient;

class ClientTest extends \PHPUnit_Framework_TestCase
{
    public function testDefaultHttpClient()
    {
        $client = new Client(new Config('login', 'pass'));
        $this->assertInstanceOf('Amass\Esputnik\Http\HttpClient', $client->getHttpClient());
    }

    public function testSetHttpClient()
    {
        $client = new Client(new Config('login', 'pass'), new HttpClient([]));
        $this->assertInstanceOf('Amass\Esputnik\Http\HttpClient', $client->getHttpClient());
    }
}
